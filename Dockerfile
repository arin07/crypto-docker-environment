FROM python:3

VOLUME /app
WORKDIR /app

# Entweder /bin/bash (bevorzugt) oder den direkten Aufruf Ihres
# Programms in Docker verwenden
CMD [ "/bin/bash"]
#CMD [ "python", "solution.py", "test.txt"]

