import sys

if len(sys.argv) < 1 :
    print("You need to specify the text file you want to analyze!")
    exit()
else:
    filename=sys.argv[1]

text=""
with open(filename,"r") as f:
    text = f.read().split("\n")

with open("solution.txt","w") as f2:
    for a in text:
        print(a)
        f2.write(a+"\n")
